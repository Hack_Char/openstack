#!/bin/bash

openstack volume type create --description 'Default volume type' --public --property volume_backend_name=os-01  os-01
openstack volume type delete __DEFAULT__
openstack volume create --size 100 testvolume

if [ ! -f cirros-0.5.2-x86_64-disk.img ]; then
	wget https://github.com/cirros-dev/cirros/releases/download/0.5.2/cirros-0.5.2-x86_64-disk.img
fi
if [[ `openstack image show cirros; echo $?` -ne 0 ]]; then
	openstack image create --community --disk-format qcow2  --file ./cirros-0.5.2-x86_64-disk.img --property os_type=linux --property os_distro=cirros --property os_admin_user=admin cirros
fi

openstack quota set --instance 100 admin &
openstack quota set --cores 100 admin &

# create initial provider network
if [ `openstack network show provider-net; echo $?` -ne 0 ]; then
	openstack network create  --external --provider-physical-network provider --provider-network-type flat provider-net && \
	openstack subnet create --network provider-net --allocation-pool start=10.1.1.100,end=10.1.1.250 --dns-nameserver 10.1.1.1 --gateway 10.1.1.1 --subnet-range 10.1.1.0/24 provider-sub &
fi

# create initial self service network
if [ `openstack network show provider-self; echo $?` -ne 0 ]; then
	openstack network create provider-service && \
	openstack subnet create --network provider-service --allocation-pool start=10.10.10.100,end=10.10.10.250 --dns-nameserver 10.1.1.1 --subnet-range 10.10.10.0/24 provider-self &
fi

# add ssh key
openstack keypair create --private-key test-key --type ssh test-key
chmod 600 ./test-key

# create default flavors
openstack flavor create --id 0  --vcpus 1 --ram 1024  --disk 20 --property hw:cpu_cores=1 --property hw:cpu_sockets=1 --property hw:cpu_threads=1 --property hw:mem_page_size=1GB c1.m1g.d20 &
openstack flavor create --id 1  --vcpus 2 --ram 2048  --disk 20 --property hw:cpu_cores=1 --property hw:cpu_sockets=1 --property hw:cpu_threads=2 --property hw:mem_page_size=1GB c2.m2g.d20 &
openstack flavor create --id 2  --vcpus 2 --ram 4096  --disk 20 --property hw:cpu_cores=1 --property hw:cpu_sockets=1 --property hw:cpu_threads=2 --property hw:mem_page_size=1GB c2.m4g.d20 &
openstack flavor create --id 3  --vcpus 4 --ram 8192  --disk 20 --property hw:cpu_cores=2 --property hw:cpu_sockets=1 --property hw:cpu_threads=2 --property hw:mem_page_size=1GB c4.m8g.d20 &

# remove quota for volumes
openstack quota set --volumes -1 admin &

# security groups
if [ `openstack security group show ssh; echo $?` -ne 0 ]; then
	openstack security group create ssh && \
	openstack security group rule create --remote-ip 0.0.0.0/0 --dst-port 22 --protocol tcp --ingress ssh &
fi

wait

# add a router between self and provider
openstack router create router && \
openstack router add subnet router provider-self && \
openstack router set router --external-gateway provider-net &

openstack floating ip create --subnet provider-sub --floating-ip-address 10.1.1.50 --tag name=provider-test-1 --tag net=provider provider-net
openstack floating ip create --subnet provider-sub --floating-ip-address 10.1.1.51 --tag name=provider-test-2 --tag net=provider provider-net

openstack hypervisor list
docker exec openstack_nova-api_1 nova-manage cell_v2 discover_hosts --verbose &

if [ `openstack server show test-server-2; echo $?` -ne 0 ]; then
	openstack server create --flavor c1.m1g.d20 --image cirros --network provider-service --security-group ssh --key-name test-key test-server-2
	while [[ `openstack port list --server test-server-2 --network provider-service -f value -c id | wc -c` -lt 10 ]]; do
                echo Waiting for test-server-2 to get a port
                sleep 2
        done
	openstack server add floating ip test-server-2 10.1.1.51
fi

if [ `openstack server show test-server-1; echo $?` -ne 0 ]; then
	openstack server create --flavor c1.m1g.d20 --image cirros --network provider-service --boot-from-volume 20 --security-group ssh --key-name test-key test-server-1
	while [[ `openstack port list --server test-server-1 --network provider-service -f value -c id | wc -c` -lt 10 ]]; do
		echo Waiting for test-server-1 to get a port
		sleep 2
	done
	openstack server add floating ip test-server-1 10.1.1.50
fi
