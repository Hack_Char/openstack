#!/bin/bash

# takes args <user> <project>
#
# generate <user>.sh from .env

. ./.env

if [ -f ./${1}.sh ]; then
	echo ${1}.sh exists
else
	USER_PASS=`pwgen -s 32 1`

	openstack user show $1 >& /dev/null
	if [[ $? == 0 ]]; then
		echo Error! User $1 already exists
		exit -1
	fi

	openstack project show $2 >& /dev/null
	if [[ $? == 0 ]]; then
		echo Project $2 exists
	else
		openstack project create $2
	fi

	openstack user create --project $2 --password ${USER_PASS} $1

	openstack role add --user $1 --project $2 member

	echo "export OS_USERNAME=${1}" > ./${1}.sh
	echo "export OS_PASSWORD=${USER_PASS}" >> ./${1}.sh
	echo "export OS_PROJECT_NAME=${2}" >> ./${1}.sh
	echo "export OS_USER_DOMAIN_NAME=Default" >> ./${1}.sh
	echo "export OS_PROJECT_DOMAIN_NAME=Default" >> ./${1}.sh
	echo "export OS_AUTH_URL=http://$CONTROLLER_IP:5000/v3" >> ./${1}.sh
	echo "export OS_IDENTITY_API_VERSION=3" >> ./${1}.sh
fi
