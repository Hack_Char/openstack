#!/bin/bash

#
#
# generate admin.sh from .env

. ./.env

if [ -f ./admin.sh ]; then
	echo admin.sh exists
else
	echo "export OS_USERNAME=admin" > ./admin.sh
	echo "export OS_PASSWORD=$ADMIN_PASS" >> ./admin.sh
	echo "export OS_PROJECT_NAME=admin" >> ./admin.sh
	echo "export OS_USER_DOMAIN_NAME=Default" >> ./admin.sh
	echo "export OS_PROJECT_DOMAIN_NAME=Default" >> ./admin.sh
	echo "export OS_AUTH_URL=http://$CONTROLLER_IP:5000/v3" >> ./admin.sh
	echo "export OS_IDENTITY_API_VERSION=3" >> ./admin.sh
fi
