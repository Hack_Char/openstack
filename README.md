# OpenStack 2023.1

https://openstack.org

## Getting started

This OpenStack configuration is designed to run on a single computer, although with the option of using more than one.

This repository (will have) the necessary information to pre-configure the bare host and bring up the needed containers.
It will pull from my Docker Hub repository the latest single-image OpenStack deployment designed to work with this project.

Also see https://hack-char.dev/openstack 

## Docker set up

Copy "env" to ".env" and replace the required PASSWORDs and the RabbitMQ cookie (need the same cookie throughout a cluster).
This docker-compose file will pull the minimized single container image from Docker Hub.

## Usage notes

While this container is currently running in my home lab, the steps in this repository have not been fully vetted. I deploy
using Ansible and have tried to document as much as needed here to configure and run this on your own machine.

## Host configuration

The information here is intended to configure a Debian 12 bare host. Use other distributions at your own risk.

There must be DNS resolution for the deployment to work. Steps for that are not provided here. Do that first.

The steps here are transcribed from an Ansible script - they have not been manually run to verify.

1) Ensure the contrib repository is added to apt and do an update/upgrade

2) Replace this line in /etc/default/grub setting the number of huge pages you want
```
GRUB_CMDLINE_LINUX="intel_iommu=on default_hugepagesz=1G hugepagesz=1G hugepages={{ HUGEPAGE_NUM }} transparent_hugepage=never"
```

3) Run /usr/sbin/update-grub if the above line was modified

4) Populate /etc/resolv.conf with your DNS server that correctly identifies your hosts
```
nameserver {{ NAMESERVER }}
```

5) Ensure /etc/hosts has proper name resolution for your machines. Requied to get RabbitMQ working correctly.

6) Mask the resolvconf package or remove it

7) Make sure the system was rebooted if a new kernel were installed

8) Make sure these packages are installed (tmux is a personal preference)
```
'docker-compose', 'docker.io', 'python3-docker', 'openvswitch-switch', 'bridge-utils', 'python3-pip', 'ifenslave', 'tmux', 'keepalived', 'haproxy', 'python3-openstackclient'
```

The following subsection is required when running a Cinder volume host (ZFS):

9a) Install these packages
```
'zfsutils-linux', 'python3-rtslib-fb', 'targetcli-fb', 'zfs-dkms'
```

9b) Create the file /etc/modprobe.d/zfs.conf with the following to limit how much memory ZFS uses (in bytes, for example '8589934592')
```
options zfs zfs_arc_max={{ ZFS_MEM }}
```

9c) Create your zpool(s) as you want. For example (run at your own risk!)
```
/usr/sbin/zpool create -f -O mountpoint=none zpool /dev/sda
```

9d) Create the needed zfs structure. For example (run at your own risk!)
```
/usr/sbin/zfs create zpool/cinder
```
10) Create an 'instances' and 'images' directories optionally zfs backed. Docker will bind mount these, see docker-compose.yml.

11) Add network interfaces /etc/network/interfaces and make sure any other is removed (example below)
```
auto lo
iface lo inet loopback
iface eno1 inet manual
auto bond0
iface bond0 inet static
  bond-slaves eno1
  address 0.0.0.0
  up ifconfig bond0 0.0.0.0 up
  bond-mode active-backup
  bond-miimon 100
  bond-downdelay 200
  bond-updelay 200
# configure vlans that are defined with open-vswitch
# DO NOT DEFINE VLANS here, just assign address & MTU
# VLANs should be defined in open vswitch
allow-hotplug data
iface data inet static
        address 10.1.1.1
        netmask 255.255.255.0
        mtu 8950
        gateway 10.1.1.1
```

12) Add keepalived configuration to /etc/keepalived/keepalived.conf (example below)
```
! OpenStack and related services
vrrp_instance VI_1 {
    interface data
    virtual_router_id 10
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass --PASSWORD--
    }
    virtual_ipaddress {
        10.1.1.2/32
    }
}
```

13) Add the HAproxy configuration to /etc/haproxy/haproxy.cfg (example in repo)

14) Create the provider interface:
```
/usr/bin/ovs-vsctl add-br provider
```

15) Configure the docker daemon /etc/docker/daemon.json (example below)
```
{
  "dns": ["10.1.1.1"],
  "default-ulimits": {
    "nofile": {
       "Name": "nofile",
       "Hard": 64000,
       "Soft": 64000
    }
  },
  "experimental": true
}
```

16) Create the docker network defined in the docker compose file
```
docker network create --subnet {{ DOCKER.SUBNET }} --opt com.docker.network.driver.mtu=8950 --opt com.docker.network.bridge.name={{ DOCKER.INTERFACE }} intnet
```

17) Restart the docker daemon if the daemon.json was updated

18) The following is very critical especially if running remote as it can cause the machine to stop working.
```
/usr/bin/systemctl restart networking; sleep 1; /usr/bin/ovs-vsctl add-port provider bond0; /usr/bin/ovs-vsctl add-br data provider 0
```

If all of the above were correctly configured, and the .env file populated correctly, then
```
docker-compose up -d
```

